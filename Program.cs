﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComplexAddition
{
    class CompAddition
    {
        public string num1;
        int real1,imag1;
        public CompAddition()
        {
                                                                    //default Constructor
        }
        public CompAddition(string n1)
        {
            this.num1 = n1;
            string[] num1Arr = num1.Split('+');
            real1 = Convert.ToInt32(num1Arr[0]);                 //retreiving real and imaginary parts 
            string s1 = num1Arr[1];                              //from the input
            string simag1 = s1.Remove(s1.Length - 1, 1);
            imag1 = Convert.ToInt32(simag1);
            
        }
        public void Print()
        {
            Console.WriteLine(" The Complex Addition is {0}+{1}i",real1,imag1);          //printing method
        }
        public static CompAddition operator+ (CompAddition a,CompAddition b)
        {
            CompAddition c = new CompAddition();
            c.real1 = a.real1 + b.real1;                                            //operator overloading method
            c.imag1 = a.imag1 + b.imag1;
            return c;

        }
    }
    class MainClass
    {
        static void Main()
        {
            
            CompAddition c1 = new CompAddition("5+6i");
            CompAddition c2 = new CompAddition("1+2i");                             //creating objects
            CompAddition c3 = c1 + c2;                                              //Adding objects using Operator overaloading
            
            c3.Print();                                                                //Printing the data 

        }
    }
}

